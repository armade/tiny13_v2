/*
   ___ _                _ _            _           _                __ _           __ _ _           
  / __\ |__   __ _ _ __| (_) ___ _ __ | | _____  _(_)_ __   __ _   / _(_)_ __ ___ / _| (_) ___  ___ 
 / /  | '_ \ / _` | '__| | |/ _ \ '_ \| |/ _ \ \/ / | '_ \ / _` | | |_| | '__/ _ \ |_| | |/ _ \/ __|
/ /___| | | | (_| | |  | | |  __/ |_) | |  __/>  <| | | | | (_| | |  _| | | |  __/  _| | |  __/\__ \
\____/|_| |_|\__,_|_|  |_|_|\___| .__/|_|\___/_/\_\_|_| |_|\__, | |_| |_|_|  \___|_| |_|_|\___||___/
                                |_|                        |___/                                    

*/
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/power.h> 
#include <util/delay.h>

#define nr_of_leds	12

#define ROR(x) (((x) >> 1) | ((x) << 7))
#define LED_ON(x)  do {PORTB |= chrlp[(x)].PORT; DDRB |= chrlp[(x)].DDR; } while(0) 
#define LED_OFF()  do {DDRB &= ~LED_MASK; PORTB &= ~LED_MASK;} while(0)

#define SLOWNESS 18                     // how infrequently we should try changing brightness
#define ACCEL 56                        // chance of actually changing the brightness (higher - less likely)
#define DIM_L 58                        // chance of staying dim (ACCEL+1 is highest)



#define AMBIENT_LIGHT 300 	//controls the level of darkness to trigger on
#define ADC2 2 
#define LIGHT_SENSOR_LED (1<<4) 


register uint8_t occount asm("r3");
register uint8_t led_i   asm("r4");
register uint8_t s       asm("r5"); // lcg var
register uint8_t a       asm("r6"); // lcg var  

 
volatile uint8_t sleep_deep;
static volatile uint8_t sleep = 0;

#define TRUE 1
#define FALSE 0

typedef struct {
	uint8_t PORT : 4;
	uint8_t DDR  : 4;
} pin_ctrl_t;

// charlieplexing configurations of B: 4 high bits - PORTB; 4 low bits - DDRB
#if (nr_of_leds==6)
pin_ctrl_t chrlp[] = {
    {.PORT = 0b001, .DDR = 0b011}, // hw led6
	{.PORT = 0b010, .DDR = 0b011}, // hw led1
	{.PORT = 0b001, .DDR = 0b101}, // hw led8
	{.PORT = 0b100, .DDR = 0b101}, // hw led2
	{.PORT = 0b010, .DDR = 0b110}, // hw led9
	{.PORT = 0b100, .DDR = 0b110}, // hw led4
}
#define LED_MASK 0b111
#endif

#if (nr_of_leds == 12)
//4 pin charlieplexing configuration
pin_ctrl_t chrlp[] = {
    {.PORT = 0b0001, .DDR = 0b0011}, // hw led6
	{.PORT = 0b0010, .DDR = 0b0011}, // hw led1
	{.PORT = 0b0001, .DDR = 0b0101}, // hw led8
	{.PORT = 0b0100, .DDR = 0b0101}, // hw led2
	{.PORT = 0b0010, .DDR = 0b0110}, // hw led9
	{.PORT = 0b0100, .DDR = 0b0110}, // hw led4
	
	{.PORT = 0b0010, .DDR = 0b1010}, // hw led11
	{.PORT = 0b1000, .DDR = 0b1010}, // hw led5
	{.PORT = 0b0100, .DDR = 0b1100}, // hw led12
	{.PORT = 0b1000, .DDR = 0b1100}, // hw led7
	{.PORT = 0b0001, .DDR = 0b1001}, // hw led10
	{.PORT = 0b1000, .DDR = 0b1001}, // hw led3
};

#define LED_MASK 0b1111
#endif

// hand picked gamma corrected brightness levels
uint8_t ramp[] = {0,1,5,7,15,23,50,73,112,254,112,73,50,23,15,7,5,1,0};

// per-led indexes in ramp array
uint8_t brightness[nr_of_leds] = {0};


static uint8_t rand(void) {
	s ^= s << 3;
	s ^= s >> 5;
	s ^= a++ >> 2;
	return s;
}

void enter_deep_sleep(void)
{
	/* Sleep for one Watchdog Timer period */
	wdt_reset();     // Reset Wathdog Timer to ensure sleep for one whole Watchdog Timer period
	WDTCR |= (1<<WDTIE);    // Enable Watchdog Interrupt Mode
	power_timer0_disable(); // Turn timer off
	/* Enable Power Down Sleep Mode */
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_mode();
	power_timer0_enable(); // Turn timer on
}


ISR(TIM0_COMPA_vect) {
	TCNT0 = 0;
	OCR0A = ROR(OCR0A);                     // decrease next timer delay by ^2, shift bit index in brightness level
	if (OCR0A & ramp[brightness[led_i]]) {  // OCR0A cycles through brightness level bits
		LED_ON(led_i);                      // if current bit is set, turn on the led
		} else {
		LED_OFF();                          // or off if not
	}
	if (OCR0A == 0b10000000) {                      
		// cycled through all current led's brightness bits
		if (led_i < (nr_of_leds-1)) 
			++led_i; 
		else 
			led_i = 0;     // select next led
			
		if (++occount > SLOWNESS) {                 // how often we should change led brightness
			for (uint8_t i = 0; i < sizeof(brightness); ++i) {
				uint8_t r = rand(); // NOLINT
				if (r > ACCEL) {                                        // chance advancing brightness
					if ((r > DIM_L)  && (brightness[i] == 0)) goto check_sleep;//continue; // chance of staying in off state
					if (brightness[i] < sizeof(ramp)-1) 
						++brightness[i]; 
					else 
						brightness[i] = 0;
				}
				check_sleep:
				if(brightness[i]==0) // If all leds brigthness == 0 then sleep for 16 ms
					sleep_deep += 1;
			}
			occount = 0;
		}
	}
}

uint16_t readADC(void) // This function reads the phototransistor through the analog/digital converter
{
	uint16_t sum = 0;
	uint8_t i;
	
	// Turn power to ADC back on
	power_adc_enable();

	// enable ADC
	ADCSRA |= (1 << ADEN);
	
	// Accumulate voltage across LED
	DDRB &= ~LIGHT_SENSOR_LED;
	_delay_ms(5);
	
	// Warm up the ADC, discard the first conversion
	ADCSRA |= (1<<ADSC); //ADC Start Conversion
	loop_until_bit_is_clear(ADCSRA,ADSC);

	for (i=0 ; i<4 ; i++) // 4x oversampling loop
	{
		_delay_ms(5);
		ADCSRA |= (1<<ADSC); //ADC Start Conversion
		loop_until_bit_is_clear(ADCSRA,ADSC); 
		sum += ADC;
		s += ADC; // Making the random generator more random
		a ^= ADC;
	}

	// The ADC har a buildin capacitor.
	// If we do not sink this the measurements will be static.
	DDRB |= LIGHT_SENSOR_LED;    // re-enable led
	ADCSRA &= ~(1<<ADEN); //ADC Disable
	//Completely turn off ADC module, reduce power consumption while sleeping.
	power_adc_disable();
	sum = sum>>2; //Average the value from the 8x oversample
	
	return sum; //return the attained value
	//return test_tmp;
}

ISR (WDT_vect) //run each time the WDT trips
{ 

	sleep = (readADC() > AMBIENT_LIGHT) ? TRUE : FALSE;

}

/* Initialization routine */
void WDT_Initialization_as_Wakeup_Source(void)
{
	/* Setup Watchdog */
	// Use Timed Sequence for disabling Watchdog System Reset Mode if it has been enabled unintentionally.
	MCUSR  &=  ~(1<<WDRF);						// Clear WDRF if it has been unintentionally set.
	WDTCR   =   (1<<WDCE )|(1<<WDE  );			// Enable configuration change.
	WDTCR   =   (1<<WDTIF)|(1<<WDTIE)|			// Enable Watchdog Interrupt Mode.
	(1<<WDCE )|(0<<WDE  )|						// Disable Watchdog System Reset Mode if unintentionally enabled.
	(0<<WDP3 )|(0<<WDP2 )|(0<<WDP1)|(0<<WDP0);	// Set Watchdog Timeout period to 16 msec.

	 sleep_deep = 0;
}


void timerStart(void) //initialize the settings for the timer cycle, disable sleep, enable PWM, start the timer, and set the mode for flicker operation
{
	power_timer0_enable();
	MCUCR &= ~(1<<SM1);   //Disable Deep Sleep
	TCCR0A = (1 << WGM01);  // timer set to CTC mode
	TIMSK0 = (1 << OCIE0A); // enable CTC timer interrupt
	OCR0A = 0b10000000; 
	TCCR0B = (1 << CS01);   // timer prescaler == 8
}
/*
void timerStop(void) //Turns the timer used in pulse() off to save power when sleeping and disables pulsetime
{
	TCCR0B &= ~(1<<CS00); //Stop Timer0
	LED_OFF();
	power_timer0_disable();
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
}*/

void ADC_init(void)
{
	// prescaler 8
	ADCSRA |= (1<<ADPS1) | (1<<ADPS0);
	//ADCSRA &= ~((1<<ADPS2) | (1<<ADPS0)); //Sets the prescaler(4) for the ADC (datasheet says between 50khz and 200khz)
	// measure pb4 using internal ref
	ADMUX = (1 << REFS0) | ADC2;      
	
	power_adc_disable();  //Completely turn off ADC module, reduce power consumption while sleeping.
	DDRB |= LIGHT_SENSOR_LED;
	PORTB &= ~LIGHT_SENSOR_LED; //PortB4  pullup resistor is set to off
}


int main(void) {
	occount = 0;
	led_i = 0;

	// read lcg seed from eeprom on startup
	// Changed on every compile
	for(a=0;a<4;a++){
		s ^= eeprom_read_byte((const uint8_t *)a);
		eeprom_busy_wait();
	} 
	
	WDT_Initialization_as_Wakeup_Source();
	timerStart();

	ADC_init();

	
	set_sleep_mode(SLEEP_MODE_IDLE);    // only disable cpu and flash clk while sleeping
	
	sei();
	
	while(1) { 
#ifdef LIGHTSENSOR	
		if(sleep){
			enter_deep_sleep(); // deep sleep for 16ms
			continue;
		}
#endif			
		if(sleep_deep == nr_of_leds){
			enter_deep_sleep(); // deep sleep for 16ms
			sleep_deep = 0;
		}else{
			sleep_mode();// sleep until timer interrupt is triggered
		}
	 }          
}

