/*
   ___ _                _ _            _           _                                        __       _ _ 
  / __\ |__   __ _ _ __| (_) ___ _ __ | | _____  _(_)_ __   __ _   ___ _ __   _____      __/ _| __ _| | |
 / /  | '_ \ / _` | '__| | |/ _ \ '_ \| |/ _ \ \/ / | '_ \ / _` | / __| '_ \ / _ \ \ /\ / / |_ / _` | | |
/ /___| | | | (_| | |  | | |  __/ |_) | |  __/>  <| | | | | (_| | \__ \ | | | (_) \ V  V /|  _| (_| | | |
\____/|_| |_|\__,_|_|  |_|_|\___| .__/|_|\___/_/\_\_|_| |_|\__, | |___/_| |_|\___/ \_/\_/ |_|  \__,_|_|_|
                                |_|                        |___/                                         
(http://www.patorjk.com/software/taag/#p=display&f=Ogre&t=Charliplexing%20snowfall)

Description:

	12 LEDs on 4 pins(PB0-PB3) using charlieplexing and bit angle modulation(more efficient 
	PWM alternative for leds) for dimming. Light will run down from top to botton creating
	a snowfall effect.

 *  Copyright (C) Peter Mikkelsen <peter.mikkelsen84@gmail.com>
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/power.h> 
#include <util/delay.h>

#define nr_of_leds	12

#define ROR(x) (((x) >> 1) | ((x) << 7))
#define LED_ON(x)  do {PORTB |= chrlp[(x)].PORT; DDRB |= chrlp[(x)].DDR; } while(0) 
#define LED_OFF()  do {DDRB &= ~(0x0f); PORTB &= ~(0x0f);} while(0)
 
#define SLOWNESS 30                    // how infrequently we should try changing brightness

#define ADC2 2 
#define AMBIENT_LIGHT 300	//controls the level of darkness to trigger on
#define LIGHT_SENSOR_LED (1<<4) 	

register uint8_t occount asm("r3");
register uint8_t led_i   asm("r4");
register uint8_t s       asm("r5"); // lcg var
register uint8_t a       asm("r6"); // lcg var  
 
volatile uint8_t nr_of_passive_leds;

// charlieplexing configurations of 6 leds
#if(nr_of_leds == 6)
pin_ctrl_t chrlp[] = {
    {.PORT = 0b001, .DDR = 0b011}, // hw led6
	{.PORT = 0b010, .DDR = 0b011}, // hw led1
	{.PORT = 0b001, .DDR = 0b101}, // hw led8
	{.PORT = 0b100, .DDR = 0b101}, // hw led2
	{.PORT = 0b010, .DDR = 0b110}, // hw led9
	{.PORT = 0b100, .DDR = 0b110}, // hw led4
}
#endif

typedef struct {
	uint8_t PORT : 4;
	uint8_t DDR  : 4;
} pin_ctrl_t;


#if(nr_of_leds == 12)
// charlieplexing configurations of 12 leds
pin_ctrl_t chrlp[] = {
    {.PORT = 0b0001, .DDR = 0b0011}, // hw led6
	{.PORT = 0b0010, .DDR = 0b0011}, // hw led1
	{.PORT = 0b0001, .DDR = 0b0101}, // hw led8
	{.PORT = 0b0100, .DDR = 0b0101}, // hw led2
	{.PORT = 0b0010, .DDR = 0b0110}, // hw led9
	{.PORT = 0b0100, .DDR = 0b0110}, // hw led4
	
	{.PORT = 0b0010, .DDR = 0b1010}, // hw led11
	{.PORT = 0b1000, .DDR = 0b1010}, // hw led5
	{.PORT = 0b0100, .DDR = 0b1100}, // hw led12
	{.PORT = 0b1000, .DDR = 0b1100}, // hw led7
	{.PORT = 0b0001, .DDR = 0b1001}, // hw led10
	{.PORT = 0b1000, .DDR = 0b1001}, // hw led3
};
#endif

// hand picked gamma corrected brightness levels
uint8_t ramp[] = {0,1,5,7,15,23,50,73,112,254};
//uint8_t ramp[] = {0,2,7,16,32,55,87,131,185,254};

// per-led indexes in ramp array
uint8_t brightness[nr_of_leds] = {0};
	
volatile uint8_t br_index=44, sleep_timer = 2;


static uint8_t rand(void) {
	s ^= s << 3;
	s ^= s >> 5;
	s ^= a++ >> 2;
	return s;
}

void enter_deep_sleep(void)
{
	/* Sleep for one Watchdog Timer period */
	wdt_reset();     // Reset Wathdog Timer to ensure sleep for one whole Watchdog Timer period
	WDTCR |= (1<<WDTIE);    // Enable Watchdog Interrupt Mode
	power_timer0_disable(); // Turn timer off
	/* Enable Power Down Sleep Mode */
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_mode();
	power_timer0_enable(); // Turn timer on
}


ISR(TIM0_COMPA_vect) {
	TCNT0 = 0;
	OCR0A = ROR(OCR0A);                     // decrease next timer delay by ^2, shift bit index in brightness level
	if (OCR0A & ramp[brightness[led_i]]) {  // OCR0A cycles through brightness level bits
		LED_ON(led_i);                      // if current bit is set, turn on the led
		} else {
		LED_OFF();                          // or off if not
	}
	if (OCR0A == 0b10000000) {                      
		// cycled through all current led's brightness bits
		if (led_i < (nr_of_leds-1)) 
			++led_i; 
		else 
			led_i = 0;     // select next led
		
		if (++occount > SLOWNESS) { // how often we should change led brightness aka fall effect speed
			
			if(br_index<sizeof(brightness))
				brightness[br_index++]=sizeof(ramp); // -1 is handled further down
			
			nr_of_passive_leds = 0;
			
			for (uint8_t i = 0; i < sizeof(brightness); ++i) {
				
				if(brightness[i])
					--brightness[i];
				
				if(brightness[i]==0) // If all leds brigthness == 0 then sleep for 16 ms
					nr_of_passive_leds += 1;
			}
			
			if(nr_of_passive_leds == nr_of_leds){
				br_index = 44;
				
				WDTCR |= (1<<WDTIE);    // Enable Watchdog Interrupt Mode
				TCCR0B &= ~(1<<CS00); //Stop Timer0
				TIMSK0 &= ~(1 << OCIE0A); // disable CTC timer interrupt
				power_timer0_disable(); // Turn timer off
				set_sleep_mode(SLEEP_MODE_PWR_DOWN);
				sleep_timer = (rand()&0x3f)+10; // never 0 max 64
			}
			
			occount = 0;
		}
	}
}

ISR (WDT_vect) //run each time the WDT trips
{

}

/* Initialization routine */
void WDT_Initialization_as_Wakeup_Source(void)
{
	/* Setup Watchdog */
	// Use Timed Sequence for disabling Watchdog System Reset Mode if it has been enabled unintentionally.
	MCUSR  &=  ~(1<<WDRF);						// Clear WDRF if it has been unintentionally set.
	WDTCR   =   (1<<WDCE )|(1<<WDE  );			// Enable configuration change.
	WDTCR   =   (1<<WDTIF)|(1<<WDTIE)|			// Enable Watchdog Interrupt Mode.
	(1<<WDCE )|(0<<WDE  )|						// Disable Watchdog System Reset Mode if unintentionally enabled.
	(0<<WDP3 )|(0<<WDP2 )|(1<<WDP1)|(1<<WDP0);	// Set Watchdog Timeout period to 0.125 sec.
}

uint16_t readADC(void) // This function reads the phototransistor through the analog/digital converter
{
	uint16_t sum = 0;
	uint8_t i;
	
	// Turn power to ADC back on
	power_adc_enable();

	// enable ADC
	ADCSRA |= (1 << ADEN);
	
	// Accumulate voltage across LED
	DDRB &= ~LIGHT_SENSOR_LED;
	_delay_ms(5);
	
	// Warm up the ADC, discard the first conversion
	ADCSRA |= (1<<ADSC); //ADC Start Conversion
	loop_until_bit_is_clear(ADCSRA,ADSC);

	for (i=0 ; i<4 ; i++) // 4x oversampling loop
	{
		_delay_ms(5);
		ADCSRA |= (1<<ADSC); //ADC Start Conversion
		loop_until_bit_is_clear(ADCSRA,ADSC); 
		sum += ADC;
		s += ADC; // Making the random generator more random
		a ^= ADC;
	}

	// The ADC har a buildin capacitor.
	// If we do not sink this the measurements will be static.
	DDRB |= LIGHT_SENSOR_LED;    // re-enable led
	ADCSRA &= ~(1<<ADEN); //ADC Disable
	//Completely turn off ADC module, reduce power consumption while sleeping.
	power_adc_disable();
	sum = sum>>2; //Average the value from the 8x oversample
	
	return sum; //return the attained value
	//return test_tmp;
}

void timerStart(void) //initialize the settings for the timer cycle, disable sleep, enable PWM, start the timer, and set the mode for flicker operation
{
	power_timer0_enable();
	TCCR0B = (1 << CS01);   // timer prescaler = 8
	TCCR0A = (1 << WGM01);  // timer set to CTC mode
	TIMSK0 = (1 << OCIE0A); // enable CTC timer interrupt
	OCR0A = 0b10000000;     // this bit will be shifted right on every interrupt
}

void timerStop(void) //Turns the timer used in pulse() off to save power when sleeping and disables pulsetime
{
	TCCR0B &= ~(1<<CS00); //Stop Timer0
	TIMSK0 &= ~(1 << OCIE0A); // disable CTC timer interrupt
	power_timer0_disable();
}

void ADC_init(void)
{
	// prescaler 8
	ADCSRA |= (1<<ADPS1) | (1<<ADPS0);
	//ADCSRA &= ~((1<<ADPS2) | (1<<ADPS0)); //Sets the prescaler(4) for the ADC (datasheet says between 50khz and 200khz)
	// measure pb4 using internal ref
	ADMUX = (1 << REFS0) | ADC2;      
	
	power_adc_disable();  //Completely turn off ADC module, reduce power consumption while sleeping.
	DDRB |= LIGHT_SENSOR_LED;
	PORTB &= ~LIGHT_SENSOR_LED; //PortB4  pullup resistor is set to off
}	


int main(void) 
{
	occount = 0;
	led_i = 0;

	// read lcg seed from eeprom on startup
	// Changed on every compile
	for(a=0;a<4;a++){
		s ^= eeprom_read_byte(a);
		eeprom_busy_wait();
	} 

	WDT_Initialization_as_Wakeup_Source();
	timerStop();
	ADC_init();
	
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // only disable cpu and flash clk while sleeping
	
	sei();
	
	for(;;) 
	{ 	
		if(br_index == 44){
			// deep sleep for 0.125s
			sleep_mode();
			if((readADC() > AMBIENT_LIGHT)) //if it got light, go to sleep
				continue;

			if(--sleep_timer==0){
				br_index = 0;
				set_sleep_mode(SLEEP_MODE_IDLE);
				timerStart();
				WDTCR &= ~(1<<WDTIE);               // Disable Watchdog Interrupt Mode
			}
		}else{
			sleep_mode();// sleep until timer interrupt is triggered
		}
	}          
}

