/*
 * tiny_pumpkin.c
 *
 * Created: 03-10-2019 11:11:41
 * Author : pm
 */ 

#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/eeprom.h> 
#include <avr/power.h> 

static uint16_t DARKNESS = 800; 	//controls the level of darkness to trigger on

static uint16_t wait;     //duration between changes in intensity
volatile uint8_t wdtTime; 	//1 if device should be sleeping
volatile uint8_t pulseTime; //1 if device should be flickering the LEDs

register uint8_t s       asm("r5"); // lcg var
register uint8_t a       asm("r6"); // lcg var

uint16_t readADC(void);

static uint8_t rand(void) 
{
	s ^= s << 3;
	s ^= s >> 5;
	s ^= a++ >> 2;
	return s;
}

//#define max_in  256 // Top end of INPUT range
//#define max_out 256 // Top end of OUTPUT range

// exp return (int)(pow(input / max_in, gamma_var) * max_out + 0.5);
// To simplify the expression the gamma value is chosen to 3

// pow(input / max_in, gamma_var) * max_out + 0.5)
// => input^3/max_in^3 * max_out + 0.5
// => input^3/max_in^2 + 0.5
// => input^3 + (1<<15)  >> 16
// (1<<15) == + 0.5 and the bit represent 0.5. This will result in a integer increment - round up.
uint16_t gamma_corr(char input)
{
	uint32_t accu;

	accu = (uint32_t)input*input*input + (1<<15); // 27 bit max

	return (accu>>16);
}

void startDog(void) //This function keeps a low-power timer (WDT) going to reset the device after timer overflows
{
	MCUCR |= (1<<SM1);    //Enable Deep Sleep
	WDTCR |= (1<<WDTIE) | (1<<WDP3) | (1<<WDP0);   //Turn on Watchdog Timer interrupt in place of reset
	wdtTime = 1; //flags WDT as on
}

void stopDog(void) // This function disables the WDT
{
	wdt_disable(); //turns off WDT
	wdtTime = 0; //flags WDT off
}

void timerStart(void) //initialize the settings for the timer cycle, disable sleep, enable PWM, start the timer, and set the mode for flicker operation
{
	//PRR &= ~(1<<PRTIM0);
	power_timer0_enable();
	MCUCR &= ~(1<<SM1);   //Disable Deep Sleep
	//TCCR0A |= (1<<WGM00) | (1<<WGM01) | (1<<COM0A1) | (1<<COM0B1); //WGM 00 and 01 set for Fast PWM. COMA and B on for noninverted PWM.
	TCCR0B |= (1<<CS00);   //Start Timer0
	pulseTime = 1;
}

void timerStop(void) //Turns the timer used in pulse() off to save power when sleeping and disables pulsetime
{
	//TCCR0A = 0;
	//TCCR0B = 0;   //Start Timer0
	TCCR0B &= ~(1<<CS00); //Stop Timer0
	//PRR |= (1<<PRTIM0);
	power_timer0_disable();
	pulseTime = 0;
}



//Functions defined
void wdt(void) //if the it is dark out turn the WDT off and start the flicker cycle otherwise go back to sleep
{
	if (readADC() < DARKNESS) //if it got light out, go to sleep
	{
		startDog();
		timerStop();
	}
	else
	{
		stopDog();
		timerStart();
	}
}

void pulse(void)
{
	if (wait){   				// are we still waiting? continue to wait
		wait--;
		return;
	}
	
	if (readADC() < DARKNESS) //if it got light out, go to sleep
	{
		startDog();
		timerStop();
	}
	else 											//Not waiting, not light out, well then let us flicker this candle!
	{
		wait = rand();  	//determine new period to wait between changes
		OCR0A = gamma_corr(((rand()&127)+127)); //Set the Yellow LED intensity
		OCR0B = gamma_corr(((rand()&127)+127)); //Set the Red LED intensity
	}
}

ISR (WDT_vect) //run each time the WDT trips
{
	wdtTime = 1;
}

ISR (TIM0_OVF_vect)    //The candle function operates off of the interrupt for the timer0.  Each overflow ticks up the cycles count.
{
	pulseTime = 1; //ensures the flicker mode stays on.
}
 
uint16_t readADC(void) // This function reads the phototransistor through the analog/digital converter
{
	/*uint16_t sum = 0;
	uint8_t i;
	//PRR &= ~(1<<PRADC); //Turn power to ADC back on
	power_adc_enable();

	PORTB |= (1<<DDB3);
	ADCSRA |= (1<<ADEN); //ADC Enable
	

	for (i=0 ; i<8 ; i++) // 8x oversampling loop
	{
		ADCSRA |= (1<<ADSC); //ADC Start Conversion
		loop_until_bit_is_clear(ADCSRA,ADSC); 
		sum += ADC;
	}

	PORTB &= ~(1<<DDB3);
	ADCSRA &= ~(1<<ADEN); //ADC Disable
	//PRR |= (1<<PRADC);  //Completely turn off ADC module, reduce power consumption while sleeping.
	power_adc_disable();
	sum = sum>>3; //Average the value from the 8x oversample
	return sum; //return the attained value*/
	return 1000;
}

void setup(void)
{
	///////////////////////////////////////////
	// IO
	DDRB |= (1<<DDB1); //PortB1 set to output   PWM1
	DDRB |= (1<<DDB0); //PortB0 set to output   PWM0
	DDRB |= (1<<DDB3); //PortB3 set to output   Source for LDR
	PORTB &= ~(1<<DDB3); //PortB3 set to low(off)
	DDRB &= ~(1<<DDB4); //PortB4 is set to input
	PORTB &= ~(1<<DDB4); //PortB4  pullup resistor is set to off
	///////////////////////////////////////////
	// TIMER
	TIMSK0 |= (1<<TOIE0); //Enable Timer0 overflow and compare registers A and B.
	TCCR0A |= (1<<WGM00) | (1<<WGM01) | (1<<COM0A1) | (1<<COM0B1); //WGM 00 and 01 set for Fast PWM. COMA and B on for noninverted PWM.
	//PRR |= (1<<PRTIM0);
	//power_timer0_disable();
	///////////////////////////////////////////
	// ADC
	ADCSRA |= (1<<ADPS1); 
	//ADCSRA &= ~((1<<ADPS2) | (1<<ADPS0)); //Sets the prescaler(4) for the ADC (datasheet says between 50khz and 200khz)
	ADMUX |= 0b00000010; // VCC used as analog reference, the result is right adjusted, ADC2 (PB4)
	//ADCSRA &= ~(1<<ADEN); //ADC Disable
	//PRR |= (1<<PRADC);  //Completely turn off ADC module, reduce power consumption while sleeping.
	//power_adc_disable();
	///////////////////////////////////////////
	// RNG 
	s = eeprom_read_byte((uint8_t*)16);     // read lcg seed from eeprom on startup
	s+= readADC();
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)16, ++s);   // save updated seed for next boot
	eeprom_busy_wait();
	
	power_all_disable();	
} 

// 600kHz or 128kHz
// For now 600kHz but test 128kHz.... 
int main(void)
{
	setup(); 		//initialize chip registers
	sei();
	__asm__ volatile ("" ::: "memory");
	startDog(); //Put device into deep sleep and set the wake cycle

	while(1) 		//functional loop for whenever the  device wakes
	{
		if (wdtTime) wdt(); 		//Should the device be sleeping? If so, check.
		if (pulseTime) pulse();		//Should the device be flickering? Then flicker!
		sleep_mode(); 				//sleep until the timer overflows or the WDT kicks off
	}
	return 0;
}




