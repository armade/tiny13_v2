#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>


// MACROS
#define SETBIT(PORT, BIT)   PORT |= BIT
#define CLRBIT(PORT, BIT)   PORT &= ~BIT

#define R_LED_PIN     	(1<<0)
#define G_LED_PIN   	(1<<1)
#define B_LED_PIN    	(1<<2)
#define LED_ALL     	(R_LED_PIN|G_LED_PIN|B_LED_PIN)

typedef struct{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t brightness;
}leds_t;

static leds_t RGB_tmp;

uint8_t counter = 0;
volatile uint8_t effect_state = 0;
uint8_t color_state = 0;
uint16_t delay = 0;

typedef struct{
	uint8_t LED_intensity;
	uint8_t LED_acc;
	uint8_t PIN;
}LED_PIN_ctrl_t;

volatile LED_PIN_ctrl_t r_led;
volatile LED_PIN_ctrl_t g_led;
volatile LED_PIN_ctrl_t b_led;


register uint8_t s       asm("r5"); // lcg var
register uint8_t a       asm("r6"); // lcg var


static uint8_t rand(void) 
{
	s ^= s << 3;
	s ^= s >> 5;
	s ^= a++ >> 2;
	return s;
}

// exp return (int)(pow(input / max_in, gamma_var) * max_out + 0.5);
// To simplify the expression the gamma value is chosen to 3

// pow(input / max_in, gamma_var) * max_out + 0.5)
// => input^3/max_in^3 * max_out + 0.5
// => input^3/max_in^2 + 0.5
// => input^3 + (1<<15)  >> 16
// (1<<15) == + 0.5 and the bit represent 0.5. This will result in a integer increment - round up.
// NB: This function assumes max input/output is 256. In this code we get an error because a
// char is only 8 bit => 255. This means that max output is 253.
static uint8_t gamma_corr(uint8_t input)
{
	uint32_t accu;

	accu = (uint32_t)input*input*input + (1UL<<15); // 27 bit max
	accu>>=16;
	
	return accu;
}



static void LED_ctrl(volatile LED_PIN_ctrl_t *LED)
{
	uint16_t u;

	u=LED->LED_intensity;
	u+=LED->LED_acc;
	LED->LED_acc=u;
	
	if (u&256) //on
		CLRBIT(PORTB, LED->PIN);
	else //off
		SETBIT(PORTB, LED->PIN);
}




ISR(TIM0_COMPA_vect)
{
	TCNT0 = 0;
	
	LED_ctrl(&r_led);
	LED_ctrl(&g_led);
	LED_ctrl(&b_led);
}

/**
 * \brief Set value (0-255)
 */
static void value_soft_PWM_RGB(leds_t *tmp)
{
	// Divide by 256 to lowers the complexity. 
	// With gamma_corr error the max output is 252.
	// This is an acceptable tradeoff.
	r_led.LED_intensity = (uint16_t)(gamma_corr(tmp->r)*tmp->brightness)>>8; 
	g_led.LED_intensity = (uint16_t)(gamma_corr(tmp->g)*tmp->brightness)>>8;
	b_led.LED_intensity = (uint16_t)(gamma_corr(tmp->b)*tmp->brightness)>>8;
	return;
}

static void Wheel(uint8_t WheelPos, leds_t *color) 
{
    if (WheelPos < 85) {
		color->r = WheelPos * 3;
		color->g = 255 - WheelPos * 3;
		color->b = 0;
        return;
    } else if (WheelPos < 170) {
        WheelPos -= 85;
		color->r = 255 - WheelPos * 3;
		color->g = 0;
		color->b = WheelPos * 3;
        return;
    } else {
        WheelPos -= 170;
		color->r = 0;
		color->g = WheelPos * 3;
		color->b = 255 - WheelPos * 3;
        return;
    }
}

static void RGB_colorloop_run(void)
{
	if(effect_state > 5)// exit
		return;
		
	Wheel(color_state+=2,&RGB_tmp);
	RGB_tmp.brightness = 255;
	if(!color_state)
		effect_state = 6;
	
	delay = 2;
	value_soft_PWM_RGB(&RGB_tmp);

	return;
}

static void RGB_fire_run(void)
{

	if(effect_state < 5)// exit
		return;
		
	RGB_tmp.r = 255;
	RGB_tmp.g = 170;
	RGB_tmp.b = 0;
	RGB_tmp.brightness = rand()|127;

	delay = rand();

	value_soft_PWM_RGB(&RGB_tmp);
	delay &= 15;
	delay++;
	
	effect_state++;
	
	return;
}

int main(void) 
{
	s = eeprom_read_byte((uint8_t*)16);     // read lcg seed from eeprom on startup
	eeprom_busy_wait();
	s ^= OSCCAL;
	//eeprom_write_byte((uint8_t*)16, ++s);   // save updated seed for next boot
	//eeprom_busy_wait();
	
	PORTB |= LED_ALL; //PortB set to high(off)
	DDRB  |= LED_ALL; //PortB set to output
	
	r_led.PIN = R_LED_PIN;
	g_led.PIN = G_LED_PIN;
	b_led.PIN = B_LED_PIN;
	
	PRR |= (1<<PRADC);  //Completely turn off ADC module, reduce power consumption while sleeping.

// 600kHz/(6*8*256) = 49 HZ
	TCCR0B = (1 << CS01);   // timer prescaler == 8
	TCCR0A = (1 << WGM01);  // timer set to CTC mode
	TIMSK0 = (1 << OCIE0A); // enable CTC timer interrupt
	OCR0A = 0b00000110; 

	delay = 1;

	sei();

	set_sleep_mode(SLEEP_MODE_IDLE);    // only disable cpu and flash clk while sleeping
	
	while(1) { 
		//if(effect_state > 5)
		sleep_mode();// sleep until timer interrupt is triggered
		
		// 600kHz/(6*8) = 12500Hz => 80us  => 12.5 ticks = 1ms
		if(--delay==0)	{
			RGB_colorloop_run();
			RGB_fire_run();	
		}
	}          
}

