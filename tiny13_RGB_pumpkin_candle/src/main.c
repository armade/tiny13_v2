/*
   __    ___   ___                       _ _      
  /__\  / _ \ / __\   ___ __ _ _ __   __| | | ___ 
 / \// / /_\//__\//  / __/ _` | '_ \ / _` | |/ _ \
/ _  \/ /_\\/ \/  \ | (_| (_| | | | | (_| | |  __/
\/ \_/\____/\_____/  \___\__,_|_| |_|\__,_|_|\___|


http://www.patorjk.com/software/taag/#p=display&f=Ogre&t=RGB%20candle                                                 
*/
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/power.h> 

// MACROS
#define SETBIT(PORT, BIT)   PORT |= BIT
#define CLRBIT(PORT, BIT)   PORT &= ~BIT

#define R_LED_PIN     	(1<<0)
#define G_LED_PIN   	(1<<1)
#define B_LED_PIN    	(1<<2)
#define LED_ALL     	(R_LED_PIN|G_LED_PIN|B_LED_PIN)

typedef struct{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t brightness;
}leds_t;


typedef enum{
	deep_sleep_mode,
	PWM_mode,
} mode_t;

mode_t current_mode;

static leds_t RGB_tmp;


typedef struct{
	uint8_t LED_intensity;
	uint8_t LED_acc;
	uint8_t PIN;
}LED_PIN_ctrl_t;

volatile LED_PIN_ctrl_t r_led;
volatile LED_PIN_ctrl_t g_led;
volatile LED_PIN_ctrl_t b_led;

uint8_t counter = 0;
volatile uint8_t effect_state = 0;
uint16_t delay = 0;



register uint8_t s       asm("r5"); // lcg var
register uint8_t a       asm("r6"); // lcg var

static uint16_t DARKNESS = 800; 	//controls the level of darkness to trigger on

static uint8_t 
rand(void) {
	s ^= s << 3;
	s ^= s >> 5;
	s ^= a++ >> 2;
	return s;
}

static uint8_t gamma_corr(uint8_t input)
{
	uint32_t accu;

	accu = (uint32_t)input*input*input + (1UL<<15); // 27 bit max
	accu>>=16;
	
	return accu;
}

static void LED_ctrl(volatile LED_PIN_ctrl_t *LED)
{
	uint16_t u;

	u=LED->LED_intensity;
	u+=LED->LED_acc;
	LED->LED_acc=u;
	
	if (u&256) //on
		CLRBIT(PORTB, LED->PIN);
	else //off
		SETBIT(PORTB, LED->PIN);
}

/*
   |\   |\
   | \  | \
   |  \ |  \
   |   \|   \
   ___  ___
  _|  |_|  |_
*/

ISR(TIM0_COMPA_vect)
{
	TCNT0 = 0;
	
	LED_ctrl(&r_led);
	LED_ctrl(&g_led);
	LED_ctrl(&b_led);
}

ISR (WDT_vect) //run each time the WDT trips
{

}


/**
 * \brief Set value (0-255)
 */
static void value_soft_PWM_RGB(leds_t *tmp)
{
	// Divide by 256 to lowers the complexity. 
	// With gamma_corr error the max output is 252.
	// This is an acceptable tradeoff.
	r_led.LED_intensity = (uint16_t)(gamma_corr(tmp->r)*tmp->brightness)>>8; 
	g_led.LED_intensity = (uint16_t)(gamma_corr(tmp->g)*tmp->brightness)>>8;
	b_led.LED_intensity = (uint16_t)(gamma_corr(tmp->b)*tmp->brightness)>>8;
	return;
}



static void
RGB_fire_run(void)
{
	RGB_tmp.brightness = (rand() & 127)+127;

	delay = rand();

	if(delay < 5)
		delay = 5;

	value_soft_PWM_RGB(&RGB_tmp);
	delay *= 6;
	
	return;
}

void startDog(void) //This function keeps a low-power timer (WDT) going to reset the device after timer overflows
{
	MCUCR |= (1<<SM1);    //Enable Deep Sleep
	WDTCR |= (1<<WDTIE) | (1<<WDP3) | (1<<WDP0);   //Turn on Watchdog Timer interrupt in place of reset
}

void stopDog(void) // This function disables the WDT
{
	wdt_disable(); //turns off WDT
}

void timerStart(void) //initialize the settings for the timer cycle, disable sleep, enable PWM, start the timer, and set the mode for flicker operation
{
	power_timer0_enable();
	MCUCR &= ~(1<<SM1);   //Disable Deep Sleep
	TCCR0A = (1 << WGM01);  // timer set to CTC mode
	TIMSK0 = (1 << OCIE0A); // enable CTC timer interrupt
	OCR0A = 0b00000110; 
	TCCR0B = (1 << CS01);   // timer prescaler == 8
}

void timerStop(void) //Turns the timer used in pulse() off to save power when sleeping and disables pulsetime
{
	//TCCR0A = 0;
	//TCCR0B = 0;   //Start Timer0
	TCCR0B &= ~(1<<CS00); //Stop Timer0
	//PRR |= (1<<PRTIM0);
	SETBIT(PORTB, LED_ALL);
	power_timer0_disable();
}

uint16_t readADC(void) // This function reads the phototransistor through the analog/digital converter
{
	uint16_t sum = 0;
	uint8_t i;
	//PRR &= ~(1<<PRADC); //Turn power to ADC back on
	power_adc_enable();

	for (i=0 ; i<8 ; i++) // 8x oversampling loop
	{
		ADCSRA |= (1<<ADSC); //ADC Start Conversion
		loop_until_bit_is_clear(ADCSRA,ADSC); 
		sum += ADC;
	}

	ADCSRA &= ~(1<<ADEN); //ADC Disable
	//PRR |= (1<<PRADC);  //Completely turn off ADC module, reduce power consumption while sleeping.
	power_adc_disable();
	sum = sum>>3; //Average the value from the 8x oversample
	return sum; //return the attained value
	//return 1000;
}

int 
main(void) 
{
	s = eeprom_read_byte((uint8_t*)16);     // read lcg seed from eeprom on startup
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)16, ++s);   // save updated seed for next boot
	eeprom_busy_wait();
	
	DDRB |= LED_ALL; //PortB set to output
	PORTB |= (1<<DDB0) | (1<<DDB1) | (1<<DDB2); //PortB set to high(off)
	

	DDRB &= ~(1<<DDB4); //PortB4 is set to input
	PORTB &= ~(1<<DDB4); //PortB4  pullup resistor is set to off
	
	r_led.PIN = R_LED_PIN;
	g_led.PIN = G_LED_PIN;
	b_led.PIN = B_LED_PIN;
	
	// ADC
	ADCSRA |= (1<<ADPS1); 
	//ADCSRA &= ~((1<<ADPS2) | (1<<ADPS0)); //Sets the prescaler(4) for the ADC (datasheet says between 50khz and 200khz)
	ADMUX |= 0b00000010; // VCC used as analog reference, the result is right adjusted, ADC2 (PB4)

// 600kHz/(6*8*256) = 49 HZ	
	timerStart();

	delay = 1;
	RGB_tmp.r = 255;
	RGB_tmp.g = 170;
	RGB_tmp.b = 0;

	current_mode = PWM_mode;
	set_sleep_mode(SLEEP_MODE_IDLE);    // only disable cpu and flash clk while sleeping

	sei();
	
	while(1) { 
		sleep_mode();// sleep until timer interrupt is triggered
		

		if (readADC() < DARKNESS) //if it got light out, go to sleep
		{
			if(current_mode == PWM_mode)
			{
				startDog();
				timerStop();
				current_mode = deep_sleep_mode;
			}
		}
		else
		{
			if(current_mode == deep_sleep_mode)
			{
				stopDog();
				timerStart();
				delay = 1;
				current_mode = PWM_mode;
			}
			
			// 600kHz/(6*8) = 12500Hz => 80us  => 12.5 ticks = 1ms
			if(--delay==0)
			{
				RGB_fire_run();	
			}
		}	
	}          
}

