#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/power.h> 

// MACROS
#define SETBIT(PORT, BIT)   PORT |= BIT
#define CLRBIT(PORT, BIT)   PORT &= ~BIT

#define LED_RED     (1<<0)
#define LED_GREEN   (1<<1)
#define LED_BLUE    (1<<2)
#define LED_ALL     (LED_RED|LED_GREEN|LED_BLUE)

typedef struct{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t brightness;
}leds_t;

typedef union{
	leds_t led;
	uint32_t all;
}RGB_soft_t;

typedef enum{
	deep_sleep_mode,
	PWM_mode,
} mode_t;

mode_t current_mode;

uint8_t counter = 0;
volatile uint8_t effect_state = 0;
uint16_t delay = 0;

static volatile RGB_soft_t RGB; 		// True output
static volatile RGB_soft_t RGB_reload; 	// True reload output

static volatile RGB_soft_t RGB_tmp;


register uint8_t s       asm("r5"); // lcg var
register uint8_t a       asm("r6"); // lcg var

volatile uint8_t wdtTime; 
static uint16_t DARKNESS = 800; 	//controls the level of darkness to trigger on

static uint8_t 
rand(void) {
	s ^= s << 3;
	s ^= s >> 5;
	s ^= a++ >> 2;
	return s;
}

/*
   |\   |\
   | \  | \
   |  \ |  \
   |   \|   \
   ___  ___
  _|  |_|  |_
*/

ISR(TIM0_COMPA_vect)
{
	TCNT0 = 0;
	
	if(--counter == 0){
		RGB.all = RGB_reload.all;
		SETBIT(PORTB, LED_ALL);
	}else{
		if(counter == RGB.led.r)	CLRBIT(PORTB,LED_RED); 
		if(counter == RGB.led.g)	CLRBIT(PORTB,LED_GREEN);
		if(counter == RGB.led.b)	CLRBIT(PORTB,LED_BLUE);
	}
}

ISR (WDT_vect) //run each time the WDT trips
{
	wdtTime = 1;
}


/**
 * \brief Set value (0-255)
 */
static void
value_soft_PWM_RGB(RGB_soft_t tmp)
{
	uint8_t R,G,B;
	
	R = (tmp.led.r*tmp.led.brightness)>>8; 
	G = (tmp.led.g*tmp.led.brightness)>>8;
	B = (tmp.led.b*tmp.led.brightness)>>8;
	
	cli();
	RGB_reload.led.r = R;
	RGB_reload.led.g = G;
	RGB_reload.led.b = B;
	sei();
	
	return;
}



static void
RGB_fire_run(void)
{
	RGB_tmp.led.brightness = (rand() & 127)+127;

	delay = rand();

	if(delay < 5)
		delay = 5;

	value_soft_PWM_RGB(RGB_tmp);
	delay *= 6;
	
	return;
}

void startDog(void) //This function keeps a low-power timer (WDT) going to reset the device after timer overflows
{
	MCUCR |= (1<<SM1);    //Enable Deep Sleep
	WDTCR |= (1<<WDTIE) | (1<<WDP3) | (1<<WDP0);   //Turn on Watchdog Timer interrupt in place of reset
}

void stopDog(void) // This function disables the WDT
{
	wdt_disable(); //turns off WDT
}

void timerStart(void) //initialize the settings for the timer cycle, disable sleep, enable PWM, start the timer, and set the mode for flicker operation
{
	power_timer0_enable();
	MCUCR &= ~(1<<SM1);   //Disable Deep Sleep
	TCCR0A = (1 << WGM01);  // timer set to CTC mode
	TIMSK0 = (1 << OCIE0A); // enable CTC timer interrupt
	OCR0A = 0b00000110; 
	TCCR0B = (1 << CS01);   // timer prescaler == 8
}

void timerStop(void) //Turns the timer used in pulse() off to save power when sleeping and disables pulsetime
{
	//TCCR0A = 0;
	//TCCR0B = 0;   //Start Timer0
	TCCR0B &= ~(1<<CS00); //Stop Timer0
	//PRR |= (1<<PRTIM0);
	SETBIT(PORTB, LED_ALL);
	power_timer0_disable();
}

uint16_t readADC(void) // This function reads the phototransistor through the analog/digital converter
{
	uint16_t sum = 0;
	uint8_t i;
	//PRR &= ~(1<<PRADC); //Turn power to ADC back on
	power_adc_enable();

	PORTB |= (1<<DDB3);
	ADCSRA |= (1<<ADEN); //ADC Enable
	

	for (i=0 ; i<8 ; i++) // 8x oversampling loop
	{
		ADCSRA |= (1<<ADSC); //ADC Start Conversion
		loop_until_bit_is_clear(ADCSRA,ADSC); 
		sum += ADC;
	}

	PORTB &= ~(1<<DDB3);
	ADCSRA &= ~(1<<ADEN); //ADC Disable
	//PRR |= (1<<PRADC);  //Completely turn off ADC module, reduce power consumption while sleeping.
	power_adc_disable();
	sum = sum>>3; //Average the value from the 8x oversample
	return sum; //return the attained value
	//return 1000;
}

int 
main(void) 
{
	s = eeprom_read_byte((uint8_t*)16);     // read lcg seed from eeprom on startup
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)16, ++s);   // save updated seed for next boot
	eeprom_busy_wait();
	
	DDRB |= LED_ALL; //PortB set to output
	PORTB |= (1<<DDB0) | (1<<DDB1) | (1<<DDB2); //PortB set to high(off)
	
	DDRB |= (1<<DDB3); //PortB3 set to output   Source for LDR
	PORTB &= ~(1<<DDB3); //PortB3 set to low(off)
	DDRB &= ~(1<<DDB4); //PortB4 is set to input
	PORTB &= ~(1<<DDB4); //PortB4  pullup resistor is set to off
	
	// ADC
	ADCSRA |= (1<<ADPS1); 
	//ADCSRA &= ~((1<<ADPS2) | (1<<ADPS0)); //Sets the prescaler(4) for the ADC (datasheet says between 50khz and 200khz)
	ADMUX |= 0b00000010; // VCC used as analog reference, the result is right adjusted, ADC2 (PB4)

// 600kHz/(6*8*256) = 49 HZ
	
	timerStart();

	delay = 1;
	RGB_tmp.led.r = 255;
	RGB_tmp.led.g = 170;
	RGB_tmp.led.b = 0;

	sei();
	current_mode = PWM_mode;
	set_sleep_mode(SLEEP_MODE_IDLE);    // only disable cpu and flash clk while sleeping
	
	while(1) { 
		sleep_mode();// sleep until timer interrupt is triggered
		

		if (readADC() < DARKNESS) //if it got light out, go to sleep
		{
			if(current_mode == PWM_mode)
			{
				startDog();
				timerStop();
				current_mode = deep_sleep_mode;
			}
		}
		else
		{
			if(current_mode == deep_sleep_mode)
			{
				stopDog();
				timerStart();
				delay = 1;
				current_mode = PWM_mode;
			}
		}
		
		// 600kHz/(6*8) = 12500Hz => 80us  => 12.5 ticks = 1ms
		if(--delay==0)
		{
			RGB_fire_run();	
		}
	}          
}

