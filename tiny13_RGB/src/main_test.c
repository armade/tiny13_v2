#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>


// MACROS
#define SETBIT(PORT, BIT)   PORT |= BIT
#define CLRBIT(PORT, BIT)   PORT &= ~BIT

#define LED_RED     (1<<0)
#define LED_GREEN   (1<<1)
#define LED_BLUE    (1<<2)
#define LED_ALL     (LED_RED|LED_GREEN|LED_BLUE)




register uint8_t s       asm("r5"); // lcg var
register uint8_t a       asm("r6"); // lcg var



static uint8_t 
rand(void) {
	s ^= s << 3;
	s ^= s >> 5;
	s ^= a++ >> 2;
	return s;
}

void _delay_ms(uint8_t delay)
{
	uint8_t i;

	for(i=0;i<delay;i++){
		asm volatile("NOP");
		asm volatile("NOP");
		asm volatile("NOP");
	}
}



void redtoyellow(void)
{
	CLRBIT(PORTB,LED_RED);
	SETBIT(PORTB,LED_BLUE);
	// fade up green
	for(uint8_t i=1; i<100; i++) {
		uint8_t on = i;
		uint8_t off = 100-on;
		for( uint8_t a=0; a<100; a++ ) {
			CLRBIT(PORTB,LED_GREEN);
			_delay_ms(on);
			SETBIT(PORTB,LED_GREEN);
			_delay_ms(off);
		}
	}
}

void yellowtogreen(void)
{
	CLRBIT(PORTB,LED_GREEN);
	SETBIT(PORTB,LED_BLUE);
	// fade up green
	for(uint8_t i=1; i<100; i++) {
		uint8_t on = 100-i;
		uint8_t off = i;
		for( uint8_t a=0; a<100; a++ ) {
			CLRBIT(PORTB,LED_RED);
			_delay_ms(on);
			SETBIT(PORTB,LED_RED);
			_delay_ms(off);
		}
	}
}

void greentocyan(void)
{
	CLRBIT(PORTB,LED_GREEN);
	SETBIT(PORTB,LED_RED);
	// fade up green
	for(uint8_t i=1; i<100; i++) {
		uint8_t on = i;
		uint8_t off = 100-on;
		for( uint8_t a=0; a<100; a++ ) {
			CLRBIT(PORTB,LED_BLUE);
			_delay_ms(on);
			SETBIT(PORTB,LED_BLUE);
			_delay_ms(off);
		}
	}
}

void cyantoblue(void)
{
	CLRBIT(PORTB,LED_BLUE);
	SETBIT(PORTB,LED_RED);
	// fade up green
	for(uint8_t i=1; i<100; i++) {
		uint8_t on = 100-i;
		uint8_t off = i;
		for( uint8_t a=0; a<100; a++ ) {
			CLRBIT(PORTB,LED_GREEN);
			_delay_ms(on);
			SETBIT(PORTB,LED_GREEN);
			_delay_ms(off);
		}
	}
}

void bluetomajenta(void)
{
	CLRBIT(PORTB,LED_BLUE);
	SETBIT(PORTB,LED_GREEN);
	// fade up green
	for(uint8_t i=1; i<100; i++) {
		uint8_t on = i;
		uint8_t off = 100-on;
		for( uint8_t a=0; a<100; a++ ) {
			CLRBIT(PORTB,LED_RED);
			_delay_ms(on);
			SETBIT(PORTB,LED_RED);
			_delay_ms(off);
		}
	}
}

void majenatored(void)
{
	CLRBIT(PORTB,LED_RED);
	SETBIT(PORTB,LED_GREEN);
	// fade up green
	for(uint8_t i=1; i<100; i++) {
		uint8_t on = 100-i;
		uint8_t off = i;
		for( uint8_t a=0; a<100; a++ ) {
			CLRBIT(PORTB,LED_BLUE);
			_delay_ms(on);
			SETBIT(PORTB,LED_BLUE);
			_delay_ms(off);
		}
	}
}


void loop(void)
{
	redtoyellow();
	yellowtogreen();
	greentocyan();
	cyantoblue();
	bluetomajenta();
	majenatored();
}


int 
main(void) 
{
	s = eeprom_read_byte((uint8_t*)16);     // read lcg seed from eeprom on startup
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)16, ++s);   // save updated seed for next boot
	eeprom_busy_wait();
	
	DDRB |= LED_ALL; //PortB set to output
	PORTB |= (1<<DDB0) | (1<<DDB1) | (1<<DDB2); //PortB set to high(off)
	
	PRR |= (1<<PRADC);  //Completely turn off ADC module, reduce power consumption while sleeping.


	while(1){
		loop();
	}         
}

